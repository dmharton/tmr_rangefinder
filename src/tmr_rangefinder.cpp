
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/subscriber.h>
#include <iostream>
#include "opencv2/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <cv_bridge/cv_bridge.h>
#include "opencv2/core/types.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/ximgproc/disparity_filter.hpp"
#include "boost/thread.hpp"

#include <array>
using namespace std;
using namespace cv;


void readme();

void stereoImageCallback(const sensor_msgs::ImageConstPtr &,
                         const sensor_msgs::ImageConstPtr &,
                         const sensor_msgs::CameraInfoConstPtr &);

Mat get3dPoints(Mat disparity, Mat M, Mat D, Mat R, Mat T) {

    Mat R1, R2, P1, P2, Q;
    stereoRectify(M,D,M,D,disparity.size(),R,T,R1,R2,P1,P2,Q);

    Mat xyz;
    reprojectImageTo3D(disparity, xyz, Q, false, CV_32FC1);
    return xyz;
}


float getAverageDistance(Mat reprojection, int width, int height) {

    int centerX = reprojection.cols/2,
        centerY = reprojection.rows/2;

    float distMax = -87.1774f;
    vector<float> centerPoints;
    int count = 0;
    for (int i = centerX - width/2; i <= centerX + width/2; i++) {
        for (int j = centerY - height/2; j <= centerY + height/2; j++) {

            Point3f pt = reprojection.at<Point3f>(j, i);
            if (pt.z >= 0 && !isinf(pt.z)) {//positive, not infinity
                centerPoints.push_back(reprojection.at<Point3f>(j, i).z / 1000.f);
            }
            count++;
        }
    }

    float distanceSum = 0.0;
    for(float point : centerPoints) {
        distanceSum += point;
        //cout << point << ' ';
    }
    return centerPoints.size() > 0 ? distanceSum/centerPoints.size() : distMax;
}


float getMean(vector<float> nums) {

    if (nums.size() == 0){
        return 0;
    }

    float sum = 0;
    for (int i = 0; i < nums.size(); ++i) {
        sum += nums[i];
    }
    return sum / nums.size();
}


/**
 * @function main
 * @brief Main function
 */
int main(int argc, char **argv) {
    if (argc < 4) {
        cout << endl;
        for (int i = 0; i < argc; ++i) {
            cout << argv[i] << endl;
        }
        readme();
        return -1;
    }

    ros::init(argc, argv, "depthmap");
    ros::NodeHandle nh;
    startWindowThread();
    image_transport::ImageTransport it(nh);
    message_filters::Subscriber<sensor_msgs::Image> leftsub(nh, argv[1], 1);
    message_filters::Subscriber<sensor_msgs::Image> rightsub(nh, argv[2], 1);
    message_filters::Subscriber<sensor_msgs::CameraInfo> camsub(nh, argv[3], 1);

    typedef message_filters::sync_policies::ApproximateTime
            <sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::CameraInfo> approxSyncPolicy;

    // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
    message_filters::Synchronizer<approxSyncPolicy> sync(approxSyncPolicy(10), leftsub, rightsub, camsub);

    sync.setAgePenalty(1.0);
    sync.registerCallback(boost::bind(&stereoImageCallback, _1, _2, _3));


    ros::Rate loop_rate(30);
    while (nh.ok()) {
        ros::spinOnce();
        waitKey(1); //keep opencv windows alive
        loop_rate.sleep();
    }


    return 0;
}

void stereoImageCallback(const sensor_msgs::ImageConstPtr &left_msg,
                         const sensor_msgs::ImageConstPtr &right_msg,
                         const sensor_msgs::CameraInfoConstPtr &cam_msg) {

    cv_bridge::CvImageConstPtr left_image_ptr, right_image_ptr;
    try {
        left_image_ptr = cv_bridge::toCvShare(left_msg, "bgr8");
        right_image_ptr = cv_bridge::toCvShare(right_msg, "bgr8");
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", left_msg->encoding.c_str());
        return;
    }


    // Read the images

    Mat imgLeft, imgRight;
    Mat(left_image_ptr->image).convertTo(imgLeft, CV_8UC1);
    Mat(right_image_ptr->image).convertTo(imgRight, CV_8UC1);

    if (imgLeft.empty() || imgRight.empty()) {
        ROS_ERROR("Could not read images.");
        return;
    }

    // convert to grayscale
    cvtColor(imgLeft, imgLeft, CV_RGB2GRAY);
    cvtColor(imgRight, imgRight, CV_RGB2GRAY);

    // crop out blank space from rectified images.
    int offset_x = 210;
    int offset_y = 120;

    cv::Rect crop;
    crop.x = offset_x;
    crop.y = offset_y;
    crop.width = imgLeft.size().width - (offset_x * 2);
    crop.height = imgLeft.size().height - (offset_y * 2);

    imgLeft = imgLeft(crop);
    imgRight = imgRight(crop);

    // reduce image size to improve processing speed.
    resize(imgLeft, imgLeft, Size(), 0.5, 0.5);
    resize(imgRight, imgRight, Size(), 0.5, 0.5);


    imshow("imgLeft", imgLeft);



    ////////////////SBM/////////////////


    Mat sbmDisparity16S = Mat(imgLeft.rows, imgLeft.cols, CV_16S);
    Mat sbmDisparity8U = Mat(imgLeft.rows, imgLeft.cols, CV_8UC1);

    int ndisparities = 16 * 2;   /**< Range of disparity */
    int SADWindowSize = 15; /**< Size of the block window. Must be odd */

    Ptr<StereoBM> sbm = StereoBM::create(ndisparities, SADWindowSize);
    sbm->compute(imgLeft, imgRight, sbmDisparity16S);

    //-- Check its extreme values
    double minVal;
    double maxVal;

    minMaxLoc(sbmDisparity16S, &minVal, &maxVal);
    sbmDisparity16S.convertTo(sbmDisparity8U, CV_8UC1, 255 / (maxVal - minVal));

    namedWindow("sbm", WINDOW_NORMAL);
    imshow("sbm", sbmDisparity8U);



    //////////////////SGBM//////////////////

    Mat sgbmDisparity16S = Mat(imgLeft.rows, imgLeft.cols, CV_16S);
    Mat sgbmDisparity8U = Mat(imgLeft.rows, imgLeft.cols, CV_8UC1);

    Ptr<StereoSGBM> sgbm = StereoSGBM::create(0, ndisparities, SADWindowSize);
    sgbm->compute(imgLeft, imgRight, sgbmDisparity16S);

    minMaxLoc(sgbmDisparity16S, &minVal, &maxVal);
    sgbmDisparity16S.convertTo(sgbmDisparity8U, CV_8UC1, 255 / (maxVal - minVal));

    namedWindow("sgbm", WINDOW_NORMAL);
    imshow("sgbm", sgbmDisparity8U);


    //////////////////SGBM2//////////////////

    Mat sgbm2Disparity16S = Mat(imgLeft.rows, imgLeft.cols, CV_16S);
    Mat sgbm2Disparity8U = Mat(imgLeft.rows, imgLeft.cols, CV_8UC1);

    Ptr<StereoSGBM> sgbm2 = StereoSGBM::create(0, 16*2, 5,
                                               100, 1000, //p1 and p2
                                               1, //disp12MaxDiff
                                               0, //preFilterCap
                                               5, //uniquenessRatio
                                               100, //speckleWindowSize
                                               1, //speckleRange
                                               StereoSGBM::MODE_HH);
    sgbm2->compute(imgLeft, imgRight, sgbm2Disparity16S);

    minMaxLoc(sgbm2Disparity16S, &minVal, &maxVal);
    sgbm2Disparity16S.convertTo(sgbm2Disparity8U, CV_8UC1, 255 / (maxVal - minVal));

    namedWindow("sgbm2", WINDOW_NORMAL);
    imshow("sgbm2", sgbm2Disparity8U);



    ///// Get camera calibration data

    double temp_P[12];
    for (int i = 0; i < 12; i++) {
        temp_P[i] = cam_msg->P[i];
    }
    Mat P = Mat(3, 4, CV_64FC1, temp_P);

    Mat T = Mat(3, 1, CV_64FC1);
    P.col(3).copyTo(T.col(0));
    Mat M = P.colRange(0, 3); //remove the 4th column from P because it's unnecessary.


    double temp_D[5];
    for (int i = 0; i < 5; i++) {
        temp_D[i] = cam_msg->D[i];
    }
    Mat D = Mat(5, 1, CV_64FC1, temp_D);


    double temp_R[9];
    for (int i = 0; i < 9; i++) {
        temp_R[i] = cam_msg->R[i];
    }
    Mat R = Mat(3, 3, CV_64FC1, temp_R);



    ///// Get average distances for each technique

    Mat sbmRepro = get3dPoints(sbmDisparity16S,M,D,R,T);
    float sbmDist = getAverageDistance(sbmRepro, 100, 100);
    cout << "sbmDist:\t" << sbmDist  << endl;


    Mat sgbmRepro = get3dPoints(sgbmDisparity16S,M,D,R,T);
    float sgbmDist = getAverageDistance(sgbmRepro, 100, 100);
    cout << "sgbmDist:\t" << sgbmDist  << endl;



    Mat sgbm2Repro = get3dPoints(sgbm2Disparity16S,M,D,R,T);
    float sgbm2Dist = getAverageDistance(sgbm2Repro, 100, 100);
    cout << "sgbm2Dist:\t" << sgbm2Dist  << endl;



    ///////FINAL OUTPUT///////

    //get min and max differences
    vector<float> distances = {sbmDist, sgbmDist, sgbm2Dist};
    float minDiff = abs(distances[0] - distances[1]);
    float maxDiff = minDiff;
    float mean = getMean(distances);
    int maxDiffPair[2] = {0,1};
    //int
    for (int i = 0; i < distances.size(); ++i) {
        for (int j = 0; j < distances.size(); ++j) {

            if (i==j) continue;

            float diff = abs(distances[i] - distances[j]);
            if (diff < minDiff) {
                minDiff = diff;
            }
            if (diff > maxDiff) {
                maxDiff = diff;
                maxDiffPair[0] = i;
                maxDiffPair[1] = j;
            }
        }

    }

    //check for outliers and remove
    if (maxDiff > 3*minDiff) {
        int outlierIndex =
                (distances[maxDiffPair[0]] - mean) <
                (distances[maxDiffPair[1]] - mean) ? maxDiffPair[0] : maxDiffPair[1];
        //distances.erase(remove(distances.begin(), distances.end(), outlierIndex), distances.end());
        distances.erase(distances.begin() + outlierIndex);
        mean = getMean(distances);
    }

    cout << "Final distance:\t" << mean << endl;

    return;


}

/**
 * @function readme
 */
void readme() { std::cout << " Usage: rosrun tmr_rangefinder tmr_rangefinder "
                          << "<imgLeft topic> <imgRight topic> <cameraInfoRight topic>"
                          << std::endl; }