# TMR Rangefinder #
## Darren Harton ##
The purpose of this project was to provide a single, reliable depth reading from a set of stereo cameras. The system utilizes a Triple Modular Redundancy (TMR) architecture which averages readings from three different blockmatching techniques and discards any outliers.

See it in action here: https://youtu.be/AdT2XeS6It0
